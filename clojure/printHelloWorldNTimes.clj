(defn hello_word_n_times[n]
    (loop [count 0]
          (if (= count n)
            ()
            (do
              (println "Hello World")
              (recur (inc count)))))

)
