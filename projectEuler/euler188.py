import fileinput

'''
This problem is a programming version of Problem 188 from projecteuler.net

Input Format

The first line of each test file contains a single integer   the number of queries you have to process.  lines follow, each containing three integers separated by single spaces: ,  and .


Output exactly  lines with the corresponding  on each line.

Sample Input 0

1
3 3 1000000000000000000
Sample Output 0

7625597484987
'''


def teration(m, n):
    if n <= 1:
        return 1
    return m ** teration(m, n - 1)


def main():
    with fileinput.input() as f:
        for line in f:
            if fileinput.isfirstline() is True:
                numQueries = int(line)
                continue
            splitLine = line.split(' ')
            a = int(splitLine[0])
            b = int(splitLine[1])
            modM = int(splitLine[2])
            recur = recursive(a, b)
            print((a ** recur) % modM)


if __name__ == '__main__':
    pass
    # main()
