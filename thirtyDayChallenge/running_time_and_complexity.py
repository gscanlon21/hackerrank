#! python3
import unittest
# import msvcrt as wait


class PrimeNumber:
    def __init__(self):
        pass

    def isPrime(self, number):
        if number == 1:
            return ("Not prime")
        for num in range(2, number - 1):
            if float(float(number) / float(num)).is_integer():
                return ("Not prime")
        return ("Prime")


class MyTest(unittest.TestCase):
    def test_me(self):
        p = PrimeNumber()
        self.assertEqual(p.isPrime(31), ("Prime"))  # OK
        self.assertEqual(p.isPrime(33), ("Not prime"))  # OK


unittest.main()
