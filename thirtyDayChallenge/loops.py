#! python3
import unittest
import sys


class Loops:
    def __init__(self, num):
        self.num = int(num)
        self.output = ''

    def loop(self):
        for x in range(1, 11):
            answer = x * self.num
            self.output += ('%s x %s = %s\n' % (self.num, x, answer))
        return self.output


class MyTest(unittest.TestCase):
    def test_me(self):
        Loops = Loops(17)
        self.assertEqual(Loops.loop(
        ), ('17 x 1 = 17\n17 x 2 = 34\n17 x 3 = 51\n17 x 4 = 68\n17 x 5 = 85\n17 x 6 = 102\n17 x 7 = 119\n17 x 8 = 136\n17 x 9 = 153\n17 x 10 = 170\n'))  # OK


unittest.main()
