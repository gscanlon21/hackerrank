#! python3
import unittest


class Node:
    def __init__(self, data):
        self.data = data
        self.next = None


class Solution:
    def __init__(self, T):
        self.T = int(T)

    def insert(self, head, data):
        p = Node(data)
        if head is None:
            head = p
        elif head.next is None:
            head.next = p
        else:
            start = head
            while(start.next is not None):
                start = start.next
            start.next = p
        return head

    def display(self, head):
        current = head
        output = ''
        while current:
            output += current.data + ' '
            current = current.next
        return output.rstrip()

    def removeDuplicates(self, head):
        if head is not None:
            current = head
        while current.next:
            if current.data == current.next.data:
                current.next = current.next.next
            else:
                current = current.next
        return head

    def solve(self, data):
        self.data = data.split(' ')
        mylist = Solution(self.T)
        head = None
        for i in range(self.T):
            datay = self.data[i]
            head = mylist.insert(head, datay)
        head = mylist.removeDuplicates(head)
        return mylist.display(head)


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solution(6)
        self.assertEqual(s.solve('1 2 2 3 3 4'), ('1 2 3 4'))  # OK


unittest.main()
