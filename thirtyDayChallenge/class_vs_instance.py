#! python3
import unittest


class Person:
    def __init__(self, initialAge):
        if initialAge < 0:
            initialAge = 0
            print('Age is not valid, setting age to 0.')
        self.initialAge = initialAge

    def amIOld(self):
        if self.initialAge >= 18:
            return ('You are old.')
        elif 13 <= self.initialAge < 18:
            return ('You are a teenager.')
        else:
            return ('You are young.')

    def yearPasses(self):
        self.initialAge += 1


class MyTest(unittest.TestCase):
    def test_me(self):
        p = Person(17)
        self.assertEqual(p.amIOld(), ('You are a teenager.'))  # OK
        self.assertIsNone(p.yearPasses())  # OK
        self.assertEqual(p.amIOld(), ('You are old.'))  # OK
        p = Person(-1)
        self.assertEqual(p.amIOld(), ('You are young.'))  # OK
        p = Person(3)
        self.assertEqual(p.amIOld(), ('You are young.'))  # OK


unittest.main()
