#! python3
import unittest

# Doesn't actually use regex


class Solution:
    def __init__(self):
        pass

    def validEmails(self, numberEmails, emails):
        emails = emails.split('\n')
        namelist = []
        for a0 in range(numberEmails):
            contactInfo = emails[a0].split(' ')
            firstName, emailID = str(contactInfo[0]), str(contactInfo[1])
            if '@gmail.com' not in emailID:
                continue
            namelist.append(firstName)

        output = ''
        for x in sorted(namelist):
            output += str(x) + '\n'

        return output.rstrip('\n')


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solution()
        self.assertEqual(s.validEmails(6, 'riya riya@gmail.com\njulia julia@julia.me\njulia sjulia@gmail.com\njulia julia@gmail.com\nsamantha samantha@gmail.com\ntanya tanya@gmail.com'),
                         ('julia\njulia\nriya\nsamantha\ntanya'))  # OK


unittest.main()
