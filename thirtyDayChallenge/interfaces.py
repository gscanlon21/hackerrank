#! python3
import unittest


class Arithmetic:
    def __init__(self):
        pass

    def divisorSum(self, dividend):
        divisorlist = []
        outputnum = 0
        for num in range(dividend):
            num += 1
            if (dividend / num).is_integer():
                divisorlist.append(num)

        for num in range(len(divisorlist)):
            outputnum += divisorlist[num]

        return outputnum


class MyTest(unittest.TestCase):
    def test_me(self):
        a = Arithmetic()
        self.assertEqual(a.divisorSum(6), (12))  # OK
        self.assertEqual(a.divisorSum(5), (6))  # OK


unittest.main()
