import unittest


class Solution:
    def __init__(self):
        pass

    def classCanceled(self, input0, input1, input2, input3, input4):
        output = ''
        for numlectures in range(int(input0)):
            if numlectures == 1:
                input1 = input3
                input2 = input4
            latestudents = 0
            x = input1.split(' ')
            numberstudents = int(x[0])
            cancelationthreshold = int(x[1])
            lateornot = input2.split(' ')
            for studentindex in range(numberstudents):
                if int(lateornot[studentindex]) > 0:
                    latestudents += 1
            if numberstudents - latestudents >= cancelationthreshold:
                output += 'NO, '
            else:
                output += 'YES, '
        return output.rstrip(', ')


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solution()
        self.assertEqual(s.classCanceled('2', '4 3', '-1 -3 4 2', '4 2', '0 -1 2 1'), ('YES, NO'))  # OK


unittest.main()
