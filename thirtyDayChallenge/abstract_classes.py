#! python3
import unittest
from abc import ABCMeta, abstractmethod


class Book(object):
    __metaclass__ = ABCMeta

    def __init__(self, title, author):
        self.title = title
        self.author = author

    @abstractmethod
    def display():
        pass

# Write MyBook class


class MyBook(Book):
    def __init__(self, title, author, price):
        self.title = title
        self.author = author
        self.price = price

    def display(self):
        print(self.title)
        return ("Title: %s\nAuthor: %s\nPrice: %s" % (self.title, self.author, self.price))


class Solve:
    def __init__(self, title, author, price):
        self.title = title
        self.author = author
        self.price = int(price)

    def solve(self):
        new_novel = MyBook(self.title, self.author, self.price)
        return new_novel.display()


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solve('The Alchemist', 'Paulo Coelho', 248)
        self.assertEqual(s.solve(), ('Title: The Alchemist\nAuthor: Paulo Coelho\nPrice: 248'))  # OK


unittest.main()
