#! python3
import unittest


class Calculator:
    def __init__(self):
        pass

    def power(self, n, p):
        if n < 0 or p < 0:
            return ("n and p should be non-negative")
        else:
            return (n**p)


class MyTest(unittest.TestCase):
    def test_me(self):
        c = Calculator()
        self.assertEqual(c.power(3, 4), (81))  # OK
        self.assertEqual(c.power(4, 3), (64))  # OK
        self.assertEqual(c.power(-3, 4), ("n and p should be non-negative"))  # OK
        self.assertEqual(c.power(-3, -4), ("n and p should be non-negative"))  # OK


unittest.main()
