#! python3
import unittest
import sys


class Exceptions:
    def __init__(self):
        pass

    def stringToInt(self, string):
        try:
            return (int(string))
        except Exception:
            return ("Bad String")


class MyTest(unittest.TestCase):
    def test_me(self):
        e = Exceptions()
        self.assertEqual(e.stringToInt('6'), (6))  # OK
        self.assertEqual(e.stringToInt('za'), ("Bad String"))  # OK


unittest.main()
