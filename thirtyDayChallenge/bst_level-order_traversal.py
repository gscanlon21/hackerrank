#! python3
import unittest
import sys


class Node:
    def __init__(self, data):
        self.right = self.left = None
        self.data = data


class Solution:
    def insert(self, root, data):
        if root is None:
            return Node(data)
        else:
            if data <= root.data:
                cur = self.insert(root.left, data)
                root.left = cur
            else:
                cur = self.insert(root.right, data)
                root.right = cur

        return root

    def levelOrder(self, root):
        outputstring = ''
        thislevel = [root]
        while thislevel:
            nextlevel = list()
            for n in thislevel:
                outputstring += str(n.data) + ' '
                if n.left:
                    nextlevel.append(n.left)
                if n.right:
                    nextlevel.append(n.right)
            thislevel = nextlevel

        return outputstring.rstrip()

    def input(self, inpt):
        inpt = inpt.split(' ')
        root = None
        for i in range(int(inpt[0])):
            data = int(inpt[i + 1])
            root = Solution().insert(root, data)

        return Solution().levelOrder(root)


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solution()
        self.assertEqual(s.input('6 3 5 4 7 2 1'), ('3 2 5 1 4 7'))  # OK


unittest.main()
