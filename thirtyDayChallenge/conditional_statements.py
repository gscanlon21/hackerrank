#! python3
import unittest
import sys


class ConditionalStatements:
    def __init__(self, integer):
        self.integer = int(integer)

    def even_or_odd(self):
        if self.integer % 2 != 0:
            return 'Weird'
        elif self.integer > 20 or 2 <= self.integer <= 5:
            return 'Not Weird'
        else:
            return 'Weird'


class MyTest(unittest.TestCase):
    def test_me(self):
        c = ConditionalStatements(13)
        self.assertEqual(c.even_or_odd(), ('Weird'))  # OK
        c = ConditionalStatements(12)
        self.assertEqual(c.even_or_odd(), ('Weird'))  # OK
        c = ConditionalStatements(22)
        self.assertEqual(c.even_or_odd(), ('Not Weird'))  # OK


unittest.main()
