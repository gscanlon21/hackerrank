#! python3
import unittest


class Review:
    def __init__(self, inputTestCases, string):
        self.inputTestCases = int(inputTestCases)
        self.string = string.split(' ')
        self.output = ''

    def review(self):
        for num in range(self.inputTestCases):
            wordOne = ''
            wordTwo = ''
            inputy = self.string[num]
            lengthString = len(inputy)
            indice = 0
            for number in range(int(round(lengthString / 2 + .5, 1))):
                letter = inputy[indice]
                indice += 2
                wordOne += letter
            indice = 1
            for number in range(int(round(lengthString / 2, 1))):
                letter = inputy[indice]
                indice += 2
                wordTwo += letter
            self.output += ('%s %s\n' % (wordOne, wordTwo))
        return (self.output)


class MyTest(unittest.TestCase):
    def test_me(self):
        r = Review(2, 'Hacker Rank')
        self.assertEqual(r.review(), ('Hce akr\nRn ak\n'))  # OK


unittest.main()
