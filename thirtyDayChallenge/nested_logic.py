#! python3
import unittest


class Solution:
    def __init__(self):
        pass

    def libraryFees(self, string):
        string = string.replace(',', '')
        string = string.split(' ')

        day1 = int(string[0])
        month1 = int(string[1])
        year1 = int(string[2])
        day2 = int(string[3])
        month2 = int(string[4])
        year2 = int(string[5])

        if year2 < year1:
            return (10000)
        elif month2 < month1 and year1 == year2:
            return (500 * int(month1 - month2))
        elif day2 < day1 and month1 == month2 and year1 == year2:
            return (15 * int(day1 - day2))
        return (0)


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Solution()
        self.assertEqual(s.libraryFees('6 12 2017, 1 2 2017'), (5000))  # OK
        self.assertEqual(s.libraryFees('6 12 2017, 1 2 2016'), (10000))  # OK
        self.assertEqual(s.libraryFees('6 12 2017, 1 12 2017'), (75))  # OK


unittest.main()
