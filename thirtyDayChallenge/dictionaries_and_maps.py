#! python3
import unittest


class PhoneBook:
    def __init__(self, numPeople):
        self.numPeople = int(numPeople)
        self.phonebook = {}

    def addNumber(self, addContact):
        self.addContact = addContact.split('\n')
        for num in range(self.numPeople):
            contact = self.addContact[num].split(' ')
            self.phonebook[contact[0]] = contact[1]

    def checkNumber(self, getNumber):
        self.getNumber = getNumber.split('\n')
        output = ''
        for num in range(self.numPeople):
            person = self.getNumber[num]
            try:
                output += ('%s=%s\n' % (person, self.phonebook[person]))
            except Exception:
                output += ('Not Found\n')
        return output


class MyTest(unittest.TestCase):
    def test_me(self):
        p = PhoneBook(3)
        self.assertEqual(p.addNumber('sam 99912222\ntom 11122222\nharry 12299933'), (None))  # OK
        self.assertEqual(p.checkNumber('sam\nedward\nharry'),
                         ('sam=99912222\nNot Found\nharry=12299933\n'))  # OK


unittest.main()
